package com.velopayments.interview.controller;

import com.velopayments.interview.Exception.CellHasValueException;
import com.velopayments.interview.Exception.CellOutOfBoundsException;
import com.velopayments.interview.Exception.GameNotFoundException;
import com.velopayments.interview.Exception.WrongPlayerException;
import com.velopayments.interview.api.CellDTO;
import com.velopayments.interview.api.GameDTO;
import com.velopayments.interview.api.MakeMoveRequestDTO;
import com.velopayments.interview.domain.Game;
import com.velopayments.interview.enums.GAME_VALUE;
import com.velopayments.interview.helper.ObjectMapperHelper;
import com.velopayments.interview.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.PostMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/games")
public class GamesController {
	private static final Logger LOG = LoggerFactory.getLogger(GamesController.class);

	@Autowired
	GameService gameService;

	/**
	 * createGame
	 *
	 * A user can POST to this endpoint to create a new game, and we will return the unique id of that game.
	 * They can then make moves using that game id.
	 */
	@PostMapping
	public ResponseEntity<?> createGame() {
		LOG.debug("createGame");

		Game game = gameService.createGame();
		UUID gameId = game.getId();

		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(gameId).toUri();
		List<CellDTO> board = ObjectMapperHelper.createBoardDTO(game.getBoard());		
		GameDTO response = new GameDTO();
		response.setId(gameId);
		response.setPlayersTurn(game.getPlayersTurn());
		response.setBoard(board);

		return ResponseEntity.created(location).body(response);
	}

	/**
	 * makeMove
	 *
	 * A user can make a move for a particular game.
	 * This should validate the move and change the current game state.
	 * It should return the new game state to the caller
	 * @throws CellOutOfBoundsException 
	 */
	@PostMapping("/{gameId}/moves")
	public ResponseEntity<?> makeMove(@PathVariable("gameId") UUID gameId, @RequestBody MakeMoveRequestDTO makeMoveRequestDTO) throws CellOutOfBoundsException {

		if (makeMoveRequestDTO.getRow() < 0 || makeMoveRequestDTO.getRow() > 3
				|| makeMoveRequestDTO.getColumn() < 0 || makeMoveRequestDTO.getColumn() > 3) {
			// Would throw Exception for an Exception handler to deal with, for now a bad request with be returned
			//throw new CellOutOfBoundsException("Please enter a valid cell index (row 1-3 and column 1 - 3)");
			return ResponseEntity.badRequest().body("Please enter a valid cell index (row 1-3 and column 1 - 3)");
		}

		try {
			Game game = gameService.makeMove(gameId);
			GameDTO gameDTO = ObjectMapperHelper.gameToGameDTOMapper(game);
        	if (makeMoveRequestDTO.getPlayer() != gameDTO.getPlayersTurn()) {
        		// Would throw Exception for an Exception handler to deal with, for now a bad request with be returned
                // throw new WrongPlayerException("It is not your turn");
        		return ResponseEntity.badRequest().body("It is not your turn!");
        	}
            CellDTO cell = gameDTO.getCellByLocation(makeMoveRequestDTO.getRow(), makeMoveRequestDTO.getColumn());            
            if (cell != null) {
	            if (GAME_VALUE.EMPTY.equals(cell.getValue())) {
	                cell.setValue(GAME_VALUE.valueOfDisplay(makeMoveRequestDTO.getValue()));
	                gameDTO.setPlayersTurn(gameDTO.getPlayersTurn()==1?2:1);
	                Game saveGame = ObjectMapperHelper.gameDTOToGameMapper(gameDTO);
	                return ResponseEntity.ok(ObjectMapperHelper.gameToGameDTOMapper(gameService.updateGame(saveGame)));
	            } else {
	            	// Would throw Exception for an Exception handler to deal with, for now a bad request with be returned
	                // throw new CellHasValueException("Cell at position " + makeMoveRequestDTO.getRow() + ", " + makeMoveRequestDTO.getColumn() + " already has a value");
	            	return ResponseEntity.badRequest().body("Cell at position " + makeMoveRequestDTO.getRow() + ", " + makeMoveRequestDTO.getColumn() + " already has a value");
	            }
            } else {
            	// Would throw Exception for an Exception handler to deal with, for now a bad request with be returned
                // throw new CellOutOfBoundsException("Cell at position " + makeMoveRequestDTO.getRow() + ", " + makeMoveRequestDTO.getColumn() + " is out of bounds");
            	return ResponseEntity.badRequest().body("Cell at position " + makeMoveRequestDTO.getRow() + ", " + makeMoveRequestDTO.getColumn() + " is out of bounds");
            }
		} catch (GameNotFoundException e) {
			// Would throw Exception for an Exception handler to deal with, for now a bad request with be returned
            return ResponseEntity.badRequest().body(e.getMessage());
		}		
	}

}
