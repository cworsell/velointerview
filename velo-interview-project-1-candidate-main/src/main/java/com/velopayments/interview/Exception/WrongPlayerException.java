package com.velopayments.interview.Exception;

public class WrongPlayerException extends Exception {

	public WrongPlayerException() {
		super();
	}

	public WrongPlayerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public WrongPlayerException(String message, Throwable cause) {
		super(message, cause);
	}

	public WrongPlayerException(String message) {
		super(message);
	}

	public WrongPlayerException(Throwable cause) {
		super(cause);
	}
}
