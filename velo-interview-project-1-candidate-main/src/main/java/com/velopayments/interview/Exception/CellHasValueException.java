package com.velopayments.interview.Exception;

/**
 *
 * @author Craig Worsell
 */
public class CellHasValueException extends Exception {

    public CellHasValueException() {
    }

    public CellHasValueException(String message) {
        super(message);
    }

    public CellHasValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public CellHasValueException(Throwable cause) {
        super(cause);
    }

    public CellHasValueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
