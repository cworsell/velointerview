package com.velopayments.interview.Exception;

/**
 *
 * @author Craig Worsell
 */
public class CellOutOfBoundsException extends Exception {

    public CellOutOfBoundsException() {
    }

    public CellOutOfBoundsException(String message) {
        super(message);
    }

    public CellOutOfBoundsException(String message, Throwable cause) {
        super(message, cause);
    }

    public CellOutOfBoundsException(Throwable cause) {
        super(cause);
    }

    public CellOutOfBoundsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
