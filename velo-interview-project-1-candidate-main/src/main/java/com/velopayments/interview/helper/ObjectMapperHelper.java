package com.velopayments.interview.helper;

import java.util.ArrayList;
import java.util.List;

import com.velopayments.interview.api.CellDTO;
import com.velopayments.interview.api.GameDTO;
import com.velopayments.interview.domain.Cell;
import com.velopayments.interview.domain.Game;

public class ObjectMapperHelper {

	public static GameDTO gameToGameDTOMapper(Game game) {
    	GameDTO dto = new GameDTO();
    	dto.setId(game.getId());
    	dto.setBoard(createBoardDTO(game.getBoard()));
    	dto.setPlayersTurn(game.getPlayersTurn());
    	return dto;
    }
    
	public static Game gameDTOToGameMapper(GameDTO gameDTO) {
    	Game game = new Game();
    	game.setId(gameDTO.getId());    	
    	game.setBoard(createBoard(gameDTO.getBoard()));
    	game.setPlayersTurn(gameDTO.getPlayersTurn());
    	return game;
    }
    
	private static CellDTO cellToCellDTOMapper(Cell cell) {
    	CellDTO dto = new CellDTO();
    	dto.setId(cell.getId());
    	dto.setGame(cell.getGame());
    	dto.setRowIndex(cell.getRowIndex());
    	dto.setColumnIndex(cell.getColumnIndex());
    	dto.setValue(cell.getValue());
    	return dto;
    }
    
	private static Cell cellDTOToCellMapper(CellDTO dto) {
    	Cell cell = new Cell();
    	cell.setId(dto.getId());
    	cell.setGame(dto.getGame());
    	cell.setRowIndex(dto.getRowIndex());
    	cell.setColumnIndex(dto.getColumnIndex());
    	cell.setValue(dto.getValue());
    	return cell;
    }
    
	public static List<CellDTO> createBoardDTO(List<Cell> board) {
    	List<CellDTO> dtos = new ArrayList<>();
    	board.forEach(c -> {
    		dtos.add(cellToCellDTOMapper(c));
    	});
    	return dtos;
    }
    
	public static List<Cell> createBoard(List<CellDTO> board) {
    	List<Cell> cells = new ArrayList<>();
    	board.forEach(c -> {
    		cells.add(cellDTOToCellMapper(c));
    	});
    	return cells;
    }
}
