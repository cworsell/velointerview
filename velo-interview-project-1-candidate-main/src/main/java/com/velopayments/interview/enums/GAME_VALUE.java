package com.velopayments.interview.enums;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author rockc
 */
public enum GAME_VALUE {
    NOUGHT("O"), CROSS("X"), EMPTY("-");
    
    public static final Map<String, GAME_VALUE> BY_DISPLAY = new HashMap<>();
    
    static {
        for (GAME_VALUE e: values()) {
            BY_DISPLAY.put(e.displayValue, e);
        }
    }
    
    public static GAME_VALUE valueOfDisplay(String display) {
        return BY_DISPLAY.get(display);
    }
    
    public final String displayValue;

    private GAME_VALUE(String value) {
        this.displayValue = value;
    }
}
