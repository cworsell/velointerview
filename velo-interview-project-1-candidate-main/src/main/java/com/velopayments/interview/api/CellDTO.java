package com.velopayments.interview.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.velopayments.interview.domain.Game;
import com.velopayments.interview.enums.GAME_VALUE;

public class CellDTO {

	private long id;
	@JsonIgnore
    private Game game;
    private Integer rowIndex;
    private Integer columnIndex;
    private GAME_VALUE value = GAME_VALUE.EMPTY;
    
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    
    public Game getGame() {
        return this.game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    
    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public GAME_VALUE getValue() {
        return value;
    }

    public void setValue(GAME_VALUE value) {
        this.value = value;
    }
}
