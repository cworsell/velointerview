package com.velopayments.interview.api;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GameDTO {

    private UUID id;

    private int playersTurn;
    private List<CellDTO> cells;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getPlayersTurn() {
        return playersTurn;
    }

    public void setPlayersTurn(int playersTurn) {
        this.playersTurn = playersTurn;
    }

    public List<CellDTO> getBoard() {
        return cells;
    }

    public void setBoard(List<CellDTO> board) {
        this.cells = board;
    }
    
    public CellDTO getCellByLocation(int row, int column) {
        CellDTO cell = cells.stream().filter(
                c -> row == c.getRowIndex() && column == c.getColumnIndex()).findFirst().orElse(null);
        return cell;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("" + this.id).append("\n");
        this.cells.forEach(c -> {
            if (c.getColumnIndex() == 1) {
                sb.append("\n");
            }
            sb.append(c.getValue().displayValue);
        });
        return sb.toString();
    }
}
