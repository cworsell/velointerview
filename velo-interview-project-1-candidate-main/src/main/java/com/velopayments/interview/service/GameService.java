package com.velopayments.interview.service;

import com.velopayments.interview.Exception.GameNotFoundException;
import com.velopayments.interview.domain.Cell;
import com.velopayments.interview.domain.Game;
import com.velopayments.interview.repository.CellRepository;
import com.velopayments.interview.repository.GameRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
@Transactional
public class GameService {
    private static final Logger LOG = LoggerFactory.getLogger(GameService.class);

    @Autowired
    GameRepository gameRepository;
    
    @Autowired
    CellRepository cellRepository;

    public Game createGame() {

        Game game = new Game();
        game.setPlayersTurn(1);
        List<Cell> cells = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                Cell cell = new Cell();
                cell.setRowIndex(i);
                cell.setColumnIndex(j);
                cell.setGame(game);
                Cell savedCell = cellRepository.save(cell);
                cells.add(savedCell);
            }
        }
        game.setBoard(cells);
        Game savedGame = gameRepository.save(game);
        LOG.debug(savedGame.getId().toString());
        return savedGame;
    }

    public Game makeMove(UUID gameId) throws GameNotFoundException {
        
        Optional<Game> game = gameRepository.findById(gameId);        
        if (game.isPresent()) {
        	List<Cell> cells = cellRepository.findByGameId(game.get().getId());
        	game.get().setBoard(cells);
        	return game.get();
        } else {
            throw new GameNotFoundException("The game with the id " + gameId + " cannot be found");
        }
    }
    
    public Game updateGame(Game game) {
    	List<Cell> cells = new ArrayList<>();
    	game.getBoard().forEach(c -> {
    		cells.add(cellRepository.save(c));
    	});
    	game.setBoard(cells);
    	Game savedGame = gameRepository.save(game);
		return savedGame;
    	
    }
}
