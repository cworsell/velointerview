package com.velopayments.interview.domain;

import com.velopayments.interview.enums.GAME_VALUE;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 *
 * @author Craig Worsell
 */
@Entity
public class Cell {
    
    @Id
    @GeneratedValue
    private long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "game_id")
    private Game game;
    @Min(1)
    @Max(3)
    private Integer rowIndex;
    @Min(1)
    @Max(3)
    private Integer columnIndex;
    private GAME_VALUE value = GAME_VALUE.EMPTY;
    
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
    	this.id = id;
    }
    
    public Game getGame() {
        return this.game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    
    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public GAME_VALUE getValue() {
        return value;
    }

    public void setValue(GAME_VALUE value) {
        this.value = value;
    }
}
