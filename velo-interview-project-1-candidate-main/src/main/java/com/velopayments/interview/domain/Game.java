package com.velopayments.interview.domain;

import java.util.List;
import java.util.Optional;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.UUID;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Game {

    /**
     * Primary key
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private UUID id;
    @Min(1)
    @Max(2)
    private int playersTurn;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
    private List<Cell> cells;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getPlayersTurn() {
        return playersTurn;
    }

    public void setPlayersTurn(int playersTurn) {
        this.playersTurn = playersTurn;
    }

    public List<Cell> getBoard() {
        return cells;
    }

    public void setBoard(List<Cell> cells) {
        this.cells = cells;
    }
}
