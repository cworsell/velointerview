package com.velopayments.interview.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.velopayments.interview.domain.Cell;

@Repository
public interface CellRepository extends CrudRepository<Cell, Long> {
	public List<Cell> findByGameId(UUID id);
}
