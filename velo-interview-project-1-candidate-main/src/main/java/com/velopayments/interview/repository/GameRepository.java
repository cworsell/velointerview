package com.velopayments.interview.repository;

import com.velopayments.interview.domain.Game;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
/**
 * TODO: Create this repository class
 *   You can change it to an interface, mark it as @Repository, and extend from CrudRepository
 */
public interface GameRepository extends CrudRepository<Game, UUID> {
}
