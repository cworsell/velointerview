package com.velopayments.interview.service;

import org.junit.Test;


/**
 * Service layer test
 * All dependencies should be mocked - you could use Mockito for this
 * This test should not need any Spring annotations or Spring context brought up for it to run
 */
public class GameServiceTest {
    
        
    // TODO: Implement any tests here - at least a "success" ("happy path") test and maybe some error scenarios
    //   If you run out of time, just add comments noting any additional tests you'd like to add
}
