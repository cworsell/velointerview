package com.velopayments.interview.controller;

import com.velopayments.interview.service.GameService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Test for Controller
 * Only the web layer is tested, the dependent service layer is mocked
 */
@WebMvcTest(controllers = GamesController.class)
public class GamesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    GameService gamesService;
    
    

    @Test
    public void createGame_success() throws Exception {
        
        this.mockMvc.perform(post("/games")).andExpect(status().isCreated());
        
    }


    @Test
    public void makeMove_success() throws Exception {

    	
    }

    //TODO: Add test to ensure exceptions are handled properly with all the invalid requests. Ie wrong player, wrong grid reference or value already added.

}
